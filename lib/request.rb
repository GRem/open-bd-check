# frozen_string_literal: true

require 'net/https'
require 'uri'
require 'json'

class Request
  def initialize(service)
    @uri_update = "http://api.openbd.local/v1/services/status/#{service.id}"
    @uri = URI.parse(service.uri)
  end

  def check
    @time_execution = Time.now
    http = Net::HTTP.new(@uri.host, @uri.port)
    http.use_ssl = false
    request = Net::HTTP::Get.new(@uri.request_uri)
    response = http.request(request)
    @time_execution = (@time_execution - Time.now).to_i.abs
    return response.code.to_i
  end

  def success
    send_response('success', @time_execution.to_s)
  end

  def error
    send_response('error', @time_execution.to_s)
  end

  def stop
    send_response('stop', 0.to_s)
  end

  private

  def send_response(status, execution)
    uri = URI.parse(@uri_update)
    http = Net::HTTP.new(uri.host,uri.port)
    http.use_ssl = false
    req = Net::HTTP::Post.new(uri.path, post_header)
    p "Update to state > #{status} with time > #{execution}"
    req.body = response_body(status, execution)
    http.request(req)
  end

  def post_header
    return {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer 430e00b532439c19447a61cb002fd529f5c4408700341a63ccb05d51996ebef4'
    }
  end

  def response_body(status, time)
    return {
      time: time,
      state: status
    }.to_json
  end
end
