# frozen_string_literal: true

require 'erb'
require 'yaml'

require_relative 'service'

class Settings
  attr_reader :services, :interval

  def initialize
    @config = read_yaml
    @services = []

    create_services
    create_constants
  end

  private

  def read_yaml
    yml = File.join(Dir.pwd, 'config.yml')
    return YAML.safe_load(ERB.new(File.read(yml)).result)
  end

  def create_services
    @config['services'].each do |key, service|
      @services.push Service.new(key,
                                 service['id'],
                                 service['uri'],
                                 service['code'])
    end
  end

  def create_constants
    @interval = @config['interval']
  end
end
