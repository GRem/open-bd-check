# frozen_string_literal: true

require_relative 'request'

class Service
  attr_reader :id, :uri

  def initialize(name, id, uri, code)
    @name = name
    @id = id
    @uri = uri
    @code = code.to_i
    @request = Request.new(self)
  end

  def watching
    log 'Check service status'
    if @request.check == @code
      log 'Success to access service'
      @request.success
    else
      log 'Error to access service'
      @request.error
    end
  rescue
    log 'Service is stoped'
    @request.stop
  end

  private

  def log(message)
    p "[#{@name}] #{message}"
  end
end
