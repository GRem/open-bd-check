# frozen_string_literal: true

require_relative 'lib/settings'

class Main
  def initialize
    start_monitor(Settings.new)
  end

  private

  def start_monitor(settings)
    while true do
      settings.services.each do |service|
        service.watching
      end
      sleep(settings.interval)
    end
  end
end

Main.new
